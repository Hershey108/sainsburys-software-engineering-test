# README #

This is a software engineering test project for Hershiv Haria.

# How to Run #

1. Ensure jars in *out/production/sainsburys-software-engineering-test/* are in classpath.
2. Ensure *out\production\sainsburys-software-engineering-test/* directory is in classpath.
3. Run: java com.foxhoundsolutions.PageParser