package com.foxhoundsolutions;

import org.json.simple.JsonArray;
import org.json.simple.JsonObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.math.BigDecimal;
import java.text.DecimalFormat;

/**
 * A class to pull down a Sainsbury's web page and grab information on the products listed.
 */
public class PageParser {

    public static void main(String[] args) {

        // URL to pull data from
        String pageUrl = "http://www.sainsburys.co.uk/webapp/wcs/stores/servlet/CategoryDisplay?listView=true" +
                "&orderBy=FAVOURITES_FIRST&parent_category_rn=12518&top_category=12518&langId=44&beginIndex=0" +
                "&pageSize=20&catalogId=10137&searchTerm=&categoryId=185749&listId=&storeId=10151" +
                "&promotionId=#langId=44&storeId=10151&catalogId=10137&categoryId=185749&parent_category_rn=12518" +
                "&top_category=12518&pageSize=20&orderBy=FAVOURITES_FIRST&searchTerm=&beginIndex=0&hideFilters=true";

        try {
            // Start application
            System.out.println("Running page parser");

            // Pull down HTML document to traverse
            Document ripeReady = Jsoup.connect(pageUrl).get();

            // Prepare an array to hold the details of each of the products
            JsonArray resultList = new JsonArray();

            // Prepare to total up the prices of the products
            BigDecimal total = new BigDecimal(0.00);

            // Get the relevant part of the page, the productLister section, and the gridItem children which each
            // contain an individual product
            Element productList = ripeReady.getElementsByClass("productLister").first();
            Elements products = productList.getElementsByClass("gridItem");

            // Loop through each product (li element), and process to retrieve our required data
            for (Element li : products) {

                // Create a JSON object for our product. This is going to hold the title, size, price and description
                JsonObject product = new JsonObject();

                // Grab the product name and URL from the relevant heading section, ready for the next steps
                Element productNameDetails = li.getElementsByClass("productNameAndPromotions").first();
                String itemUrl = productNameDetails.getElementsByTag("a").attr("href");
                String productName = productNameDetails.getElementsByTag("a").text();

                // Another level deeper - get the HTML document of the product page. The connect method URLencodes the
                // URL we pass in, so we have to decode it. Otherwise those pesky parentheses cause headaches.
                Document productPage = Jsoup.connect(java.net.URLDecoder.decode(itemUrl, "UTF-8")).get();

                // Pull out the price, removing any non-numerical or dot characters.
                String price = productPage.getElementsByClass("pricePerUnit").first().ownText().replaceAll("[^\\d.]", "");

                // Add the price to our total
                total = total.add(new BigDecimal(price));

                // All the remaining information is in the information section, which is handily given the id
                // "information". Fancy that.
                Element infoSection = productPage.getElementById("information");

                // Grab all the elements in the information section, because someone didn't helpfully give the
                // description field a nice id/class.
                Elements allInfo = infoSection.select("*");

                // Loop through the elements. We know the element that comes directly before the description, because it
                // has the text "Description" in the heading. So we keep an eye out for that.
                boolean next = false;
                for (Element infoElement: allInfo) {

                    if (next) {
                        // We know the last loop had the "Description" header, so this is the actual description field.
                        product.put("description", infoElement.text());
                        break;
                    }

                    if (infoElement.text().equals("Description")) {
                        // We found the Description header. Go us!
                        next = true;
                    }

                }

                // Lets populate the remaining fields with the required information
                product.put("title",productName);
                product.put("size", (productPage.html().getBytes().length/1024) + "KB");
                product.put("unit_price","price");

                // Add this product to our array, and move on to the next one.
                resultList.add(product);
            }

            // We have our products, lets add them to our results object, and add the total while we're at it.
            JsonObject result = new JsonObject();
            result.put("results", resultList);
            result.put("total", total);

            // Finally, print out our JSON object with all of our data. Smashing!
            System.out.println(result);
        } catch (Exception e) {
            // Oops, something went wrong. Hopefully we shouldn't hit this area, but if we do, let's see what the
            // problem is and then we can try to debug.
            System.out.println("Error: " + e.getMessage());
            e.printStackTrace();
        }
    }
}
